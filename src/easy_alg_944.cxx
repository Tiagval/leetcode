//Solution for problem: https://leetcode.com/problems/delete-columns-to-make-sorted/

#include <iostream>
#include <string>
#include <vector>

class Solution {
public:
    int minDeletionSize(std::vector<std::string>& strs) {
        assert(strs.size() > 0);
        unsigned int deletions = 0;
        std::vector<int> deleted;
        //We can assume all strings have the same length
        unsigned int n_cols = strs[0].size();
        for(unsigned int i = 0; i < n_cols; ++i){
            unsigned int last = 0;
            for(std::string &str: strs){
                //Skip already deleted columns
                if (std::find(deleted.begin(), deleted.end(), i) != deleted.end()) continue;
                unsigned int char_n = int(str[i]);
                if (char_n < last) {
                    ++deletions;
                    deleted.push_back(i);
                }
                last = char_n;
            }
        }
        return deletions;
    }
};

int main(){
   Solution sol;
   std::vector<std::string> test0 = {"abc", "bce", "cae"};
   std::vector<std::string> test1 = {"cba","daf","ghi"};
   std::vector<std::string> test2 = {"a", "b"};
   std::vector<std::string> test3 = {"zyx","wvu","tsr"};

   int sol0 = sol.minDeletionSize(test0);
   int sol1 = sol.minDeletionSize(test1);
   int sol2 = sol.minDeletionSize(test2);
   int sol3 = sol.minDeletionSize(test3);
   assert(sol0 == 1);
   assert(sol1 == 1);
   assert(sol2 == 0);
   assert(sol3 == 3);

   return 0;
}
