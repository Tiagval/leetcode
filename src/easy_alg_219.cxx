//Solution to problem https://leetcode.com/problems/contains-duplicate-ii/

#include <iostream>
#include <vector>
#include <map>

class Solution {
public:
    bool containsNearbyDuplicate(std::vector<int>& nums, int k) {
        std::map<int, int> num_map;
        for (int i = 0; i < nums.size(); ++i){
            auto stored_index = num_map.find(nums[i]);
            if(stored_index != num_map.end()){
                int diff = abs(stored_index->second - i);
                if(diff <= k) return true;
                num_map[nums[i]] = i;
            }
            else num_map[nums[i]] = i;
        }
        return false;
    }
};

int main(){
    Solution sol;
    std::vector<int> nums1 = {1,2,3,1};
    bool out1 = sol.containsNearbyDuplicate(nums1, 3);
    assert(out1);

    std::vector<int> nums2 = {1,0,1,1};
    bool out2 = sol.containsNearbyDuplicate(nums2, 1);
    assert(out2);

    std::vector<int> nums3 = {1,2,3,1,2,3};
    bool out3 = sol.containsNearbyDuplicate(nums3, 2);
    assert(!out3);

    std::vector<int> nums4 = {-1, -1};
    bool out4 = sol.containsNearbyDuplicate(nums4, 1);
    assert(out4);

    std::cout << "All tests passed!" << std::endl;
    return 0;
}