//Solution for problem: https://leetcode.com/problems/roman-to-integer/

#include <iostream>
#include <map>

class Solution {
    public:
        int romanToInt(std::string s) {
            std::map<char, int> values = getValuesMap();
            unsigned int total = 0;
            unsigned int i = 0;

            while (i < s.size()){
                char current = s[i];
                char next = i < s.size() - 1 ? s[i + 1] : s[i];
                if (values[current] >= values[next]){
                    total += values[current];
                    i++;
                }
                else{
                    total += values[next] - values[current];
                    i += 2;
                }
            }

            return total;
        };

        std::map<char, int> getValuesMap(){
            std::map<char, int> values;
            values['I'] = 1;
            values['V'] = 5;
            values['X'] = 10;
            values['L'] = 50;
            values['C'] = 100;
            values['D'] = 500;
            values['M'] = 1000;

            return values;
        };
};

int main(){
    Solution sol;

    int sol1 = sol.romanToInt("III");
    int sol2 = sol.romanToInt("LVIII");
    int sol3 = sol.romanToInt("MCMXCIV");

    assert(sol1 == 3);
    assert(sol2 == 58);
    assert(sol3 == 1994);

    return 0;
}
