//Solution to problem https://leetcode.com/problems/jewels-and-stones/

#include <iostream>
#include <map>
#include <vector>

class Solution {
public:
    int numJewelsInStones(std::string jewels, std::string stones) {
        std::map<char, int> count;
        for(char s: stones){
            ++count[s];
        }
        int answer = 0;
        for(char j: jewels){
            answer += count[j];
        }
        return answer;
    }
};

int main(){
    Solution sol;
    int out1 = sol.numJewelsInStones("aA", "aAAbbbb");
    assert(out1 == 3);

    int out2 = sol.numJewelsInStones("z", "ZZ");
    assert(out2 == 0);

    std::cout << "All tests passed!" << std::endl;
    return 0;
}