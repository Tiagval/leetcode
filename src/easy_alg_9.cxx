//Solution for problem: https://leetcode.com/problems/palindrome-number/

#include <iostream>

class Solution {
public:
    bool isPalindrome(int x) {
        //Reject negative numbers
        if (x < 0) return false;

        std::string number = std::to_string(x);
        unsigned int len = number.size();

        for (unsigned int i = 0; i < len; ++i) {
            if (number[i] != number[len - i - 1]) {
                return false;
            }
        }
        return true;
    }
};

int main(){
    Solution sol;

    bool sol1 = sol.isPalindrome(121);
    bool sol2 = sol.isPalindrome(-121);
    bool sol3 = sol.isPalindrome(10);

    assert(sol1);
    assert(!sol2);
    assert(!sol3);

    return 0;
}
