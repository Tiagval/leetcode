//Solution to problem https://leetcode.com/problems/two-sum/

#include <iostream>
#include <map>
#include <vector>

class Solution {
public:
    std::vector<int> twoSum(std::vector<int>& nums, int target) {
        std::map<int, int> cache;
        for (int i = 0; i < nums.size(); ++i){
            auto complementary_index = cache.find(target - nums[i]);
            if (complementary_index != cache.end()){
                std::vector<int> answer{complementary_index->second, i};
                return answer;
            }
            cache[nums[i]] = i;
        }
        return std::vector<int> {-1};
    }
};

int main(){
    Solution sol;

    std::vector<int> num1 = {2, 7, 11, 15};
    std::vector<int> ans1 = sol.twoSum(num1, 9);
    std::vector<int> sol1 = {0, 1};
    assert(ans1 == sol1);

    std::vector<int> num2 = {3, 2, 4};
    std::vector<int> ans2 = sol.twoSum(num2, 6);
    std::vector<int> sol2 = {1, 2};
    assert(ans2 == sol2);

    std::vector<int> num3 = {3, 3};
    std::vector<int> ans3 = sol.twoSum(num3, 6);
    std::vector<int> sol3 = {0, 1};
    assert(ans3 == sol3);

    std::cout << "All tests passed!" << std::endl;

    return 0;
}