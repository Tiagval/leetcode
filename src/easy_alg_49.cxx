//Solution to problem https://leetcode.com/problems/group-anagrams/

#include <iostream>
#include <map>
#include <vector>

class Solution {
public:
    std::vector<std::vector<std::string>> groupAnagrams(std::vector<std::string>& strs) {
        std::map<std::string, std::vector<std::string>> str_map;
        for(std::string &str: strs){
            std::string key = str;
            std::sort(key.begin(), key.end());
            str_map[key].push_back(str);
        }
        std::vector<std::vector<std::string>> answer;
        for (auto entry: str_map){
            std::vector<std::string> this_key;
            for(std::string str: entry.second){
                this_key.push_back(str);
            }
            answer.push_back(this_key);
        }
        return answer;
    }
};

int main(){
    Solution sol;

    std::vector<std::string> input1 = {"eat","tea","tan","ate","nat","bat"};
    std::vector<std::vector<std::string>> out1 = sol.groupAnagrams(input1);
    std::vector<std::vector<std::string>> sol1 = {{"bat"},{"nat","tan"},{"ate","eat","tea"}};
    assert(out1 == sol1);

    std::vector<std::string> input2 = {""};
    std::vector<std::vector<std::string>> out2 = sol.groupAnagrams(input2);
    std::vector<std::vector<std::string>> sol2 = {{""}};
    assert(out2 == sol2);

    std::vector<std::string> input3 = {"a"};
    std::vector<std::vector<std::string>> out3 = sol.groupAnagrams(input3);
    std::vector<std::vector<std::string>> sol3 = {{"a"}};
    assert(out3 == sol3);

    std::cout << "All tests passed!" << std::endl;
    return 0;
}