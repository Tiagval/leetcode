//Solution to problem https://leetcode.com/problems/minimum-index-sum-of-two-lists/

#include <iostream>
#include <vector>
#include <map>

class Solution {
public:
    std::vector<std::string> findRestaurant(std::vector<std::string>& list1, std::vector<std::string>& list2) {
        std::map<int, std::vector<std::string>> cache_map; //Will map index sum to string
        int lowest_index = 1001; //Problem statement sets high bound at 1000

        for (unsigned int i = 0; i < list1.size(); ++i){
            std::string current_word = list1[i];
            for (unsigned int j = 0; j < list2.size(); ++j){
                if(i + j > lowest_index) break; //Past current solution
                if(list2[j] == current_word){
                    cache_map[i + j].push_back(current_word);
                    lowest_index = i + j;
                }
            }
        }
        return cache_map[lowest_index];
    }
};

int main(){
    Solution sol;
    std::vector<std::string> list1_1 = {"Shogun","Tapioca Express","Burger King","KFC"};
    std::vector<std::string> list1_2 = {"Piatti","The Grill at Torrey Pines","Hungry Hunter Steakhouse","Shogun"};
    std::vector<std::string> out1 = sol.findRestaurant(list1_1, list1_2);
    std::vector<std::string> sol1 = {"Shogun"};
    assert(out1 == sol1);

    std::vector<std::string> list2_1 = {"Shogun","Tapioca Express","Burger King","KFC"};
    std::vector<std::string> list2_2 = {"KFC","Shogun","Burger King"};
    std::vector<std::string> out2 = sol.findRestaurant(list2_1, list2_2);
    std::vector<std::string> sol2 = {"Shogun"};
    assert(out2 == sol2);

    std::vector<std::string> list3_1 = {"happy","sad","good"};
    std::vector<std::string> list3_2 = {"sad","happy","good"};
    std::vector<std::string> out3 = sol.findRestaurant(list3_1, list3_2);
    std::sort(out3.begin(), out3.end());
    std::vector<std::string> sol3 = {"sad", "happy"};
    std::sort(sol3.begin(), sol3.end());
    assert(out3 == sol3);

    std::cout << "Passed all tests!" << std::endl;

    return 0;
}