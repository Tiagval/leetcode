//Solution to problem https://leetcode.com/problems/happy-number/
#include <iostream>
#include <map>

class Solution {
public:
    bool isHappy(int n, std::map<int, int> cache = {}) {
        if (n == 1) return true;
        if (cache.find(n) != cache.end()) return false;
        int sum = print_each_digit(n);
        cache[n] = sum;
        return isHappy(sum, cache);
    }

    int print_each_digit(int x, int sum = 0){
        if(x >= 10){
            sum += (x % 10)*(x % 10);
            return print_each_digit(x / 10, sum);
        }
        sum += x * x;
        return sum;
    }
};

int main(){
    Solution sol;
    bool ans1 = sol.isHappy(19);
    bool ans2 = sol.isHappy(2);
    bool ans3 = sol.isHappy(202);

    assert(ans1);
    assert(!ans2);
    assert(!ans3);

    std::cout << "All tests passed!" << std::endl;

    return 0;
}