//Solution of problem https://leetcode.com/problems/first-unique-character-in-a-string/

#include <iostream>
#include <map>
#include <vector>

class Solution {
public:
    int firstUniqChar(std::string s) {
        std::map<char, int> cache_map;
        std::vector<char> cache_vec;

        for(unsigned int i = 0; i < s.size(); ++i){
            char current_char = s[i];
            if(find(cache_vec.begin(), cache_vec.end(), current_char) != cache_vec.end()) continue; //char already deleted
            if(cache_map.find(current_char) == cache_map.end()){
                cache_map[current_char] = i;
            }
            else{
                cache_map.erase(current_char);
                cache_vec.push_back(current_char);
            }
        }
        if (cache_map.empty()) return -1;
        //Find the minimum value
        return min_element(cache_map.begin(), cache_map.end(), 
                   [](const auto &lhs, const auto &rhs){ return lhs.second < rhs.second;})->second;
    };
};

int main(){
    Solution sol;

    int out1 = sol.firstUniqChar("leetcode");
    assert(out1 == 0);

    int out2 = sol.firstUniqChar("loveleetcode");
    assert(out2 == 2);

    int out3 = sol.firstUniqChar("aabb");
    assert(out3 == -1);

    std::cout << "All tests passed" << std::endl;
    return 0;
}