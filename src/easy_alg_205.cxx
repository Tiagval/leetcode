//Solution to problem https://leetcode.com/problems/isomorphic-strings/

#include <iostream>
#include <map>

class Solution {
public:
    bool isIsomorphic(std::string s, std::string t) {
        if (s.size() != t.size()) return false;
        std::map<char, char> char_map;
        for (int i = 0; i < s.size(); ++i)
        {
            if (char_map.find(s[i]) != char_map.end()){
                if (char_map[s[i]] != t[i]) return false;
            }
            else{
                if(!isInKeys(t[i], char_map)) char_map[s[i]] = t[i];
                else return false;
            }
        }
        return true;
    }
    bool isInKeys(char target, std::map<char, char> char_map){
        for(auto &c: char_map){
            if(target == c.second) return true;
        }
        return false;
    }
};

int main(){
    Solution sol;
    bool ans1 = sol.isIsomorphic("egg", "add");
    assert(ans1);

    bool ans2 = sol.isIsomorphic("foo", "bar");
    assert(!ans2);

    bool ans3 = sol.isIsomorphic("paper", "title");
    assert(ans3);

    bool ans4 = sol.isIsomorphic("badc", "baba");
    assert(!ans4);

    std::cout << "All tests passed!" << std::endl;
    return 0;
}