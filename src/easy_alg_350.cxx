//Solution to problem https://leetcode.com/problems/intersection-of-two-arrays-ii/

#include <iostream>
#include <vector>
#include <map>

class Solution {
public:
    std::vector<int> intersect(std::vector<int>& nums1, std::vector<int>& nums2) {
        std::vector<int> intersection_list;
        for(int &num1: nums1){
            auto intersection = find(nums2.begin(), nums2.end(), num1);
            if(intersection != nums2.end()){
                intersection_list.push_back(num1);
                nums2.erase(intersection);
            }
        }
        return intersection_list;
    }
};

int main(){
    Solution sol;
    std::vector<int> nums1_1 = {1,2,2,1};
    std::vector<int> nums1_2 = {2,2};
    std::vector<int> out1 = sol.intersect(nums1_1, nums1_2);
    std::vector<int> sol1 = {2,2};
    assert(out1 == sol1);

    std::vector<int> nums2_1 = {4,9,5};
    std::vector<int> nums2_2 = {9,4,9,8,4};
    std::vector<int> out2 = sol.intersect(nums2_1, nums2_2);
    std::vector<int> sol2 = {4,9};
    assert(out2 == sol2);

    std::vector<int> nums3_1 = {1,2,2,1};
    std::vector<int> nums3_2 = {2};
    std::vector<int> out3 = sol.intersect(nums3_1, nums3_2);
    std::vector<int> sol3 = {2};
    assert(out3 == sol3);

    std::cout << "All tests passed!" << std::endl;
    return 0;
}