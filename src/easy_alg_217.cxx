//Solution for problem https://leetcode.com/problems/contains-duplicate/

#include <vector>
#include <map>

class Solution {
public:
    bool containsDuplicate(std::vector<int>& nums) {
        std::map<int, int> cache;

        for(int &num: nums){
            try{
                if(cache.at(num) > 0) return true;
            }
            catch(std::out_of_range & e){
                cache.insert(std::pair<int, int> (num, 1));
            }
        }
        return false;
    }
};


int main(){

    Solution sol;

    std::vector<int> arg1 = {1,2,3,1};
    std::vector<int> arg2 = {1,2,3,4};
    std::vector<int> arg3 = {1,1,1,3,3,4,3,2,4,2};

    bool sol1 = sol.containsDuplicate(arg1);
    bool sol2 = sol.containsDuplicate(arg2);
    bool sol3 = sol.containsDuplicate(arg3);

    assert(sol1);
    assert(!sol2);
    assert(sol3);

    return 0;
}
